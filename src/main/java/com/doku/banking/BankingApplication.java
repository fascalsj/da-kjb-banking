package com.doku.banking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is a Javadoc comment
 * RestApiApplication class
 */
@SpringBootApplication
public class BankingApplication {

	/**
	 * This is a Javadoc comment
	 * RestApiApplication class
	 * @param args args will be use
	 */
	public static void main(String[] args) {
		SpringApplication.run(BankingApplication.class, args);
	}

}
