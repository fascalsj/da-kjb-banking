package com.doku.banking.controller;

import com.doku.banking.dto.TransferDTO;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.request.TransferRequest;
import com.doku.banking.service.TransferService;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a Javadoc comment
 */
@Slf4j
@RestController
public class TransferController {

    @Autowired
    TransferService transferService;

    /**
     * This is a Javadoc comment
     * @param transferRequest the parameter of the class
     * @return for return transfer
     * @throws UserNotFoundException for throw usernotfound
     */
    @PostMapping(value = "/transfer", produces = "application/json")
    public Response<TransferDTO> doTransfer(@Validated @RequestBody TransferRequest transferRequest) throws UserNotFoundException {
        log.info("balance request : " + transferRequest.toString());
        return transferService.doTransfer(transferRequest);

    }
}
