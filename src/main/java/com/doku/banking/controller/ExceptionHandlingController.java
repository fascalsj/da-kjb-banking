package com.doku.banking.controller;

import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.util.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This is a Javadoc comment
 * RestApiApplication class
 */
@ControllerAdvice
public class ExceptionHandlingController {

    /**
     * This is a Javadoc comment
     * @param exception when user does not exist in database
     * @return for return Error
     */
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody Response userNotFoundException(final UserNotFoundException exception) {

        Response error = Response.builder().build();
        error.setResponseCode(HttpStatus.NOT_FOUND.toString());
        error.setResponseMessage(exception.getMessage());

        return error;
    }
}
