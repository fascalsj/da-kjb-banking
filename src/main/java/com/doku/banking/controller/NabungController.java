package com.doku.banking.controller;

import com.doku.banking.dto.NabungDTO;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.request.NabungRequest;
import com.doku.banking.service.NabungService;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a Javadoc comment
 */
@Slf4j
@RestController
public class NabungController {

    @Autowired
    NabungService nabungService;

    /**
     * This is a Javadoc comment
     * @param nabungRequest the parameter of the class
     * @return for return nabung
     * @throws UserNotFoundException for throw usernotfound
     */
    @PutMapping(value = "/nabung", produces = "application/json")
    public Response<NabungDTO> doNabung(@Validated @RequestBody NabungRequest nabungRequest) throws UserNotFoundException {
        log.info("balance request : "+nabungRequest.toString());
        return nabungService.doNabung(nabungRequest);

    }

}
