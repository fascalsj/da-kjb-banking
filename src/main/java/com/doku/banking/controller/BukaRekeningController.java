package com.doku.banking.controller;

import com.doku.banking.dto.BukaRekeningDTO;
import com.doku.banking.request.BukaRekeningRequest;
import com.doku.banking.service.BukaRekeningService;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a Javadoc comment
 */
@Slf4j
@RestController
public class BukaRekeningController {

    @Autowired
    BukaRekeningService bukaRekeningService;

    /**
     * This is a Javadoc comment
     * @param request the parameter of the class
     * @return for return bukarekening
     */
    @PostMapping(value = "/bukarekening", produces = "application/json")
    public Response<BukaRekeningDTO> doBukaRekening(@Validated @RequestBody BukaRekeningRequest request) {
        log.info("balance request : "+request.toString());
        return bukaRekeningService.doBukaRekening(request);

    }
}
