package com.doku.banking.repository;

import com.doku.banking.model.UsersDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface BukaRekeningRepository extends JpaRepository<UsersDetail,Long> {
}
