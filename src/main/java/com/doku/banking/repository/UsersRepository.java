package com.doku.banking.repository;

import com.doku.banking.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface UsersRepository extends JpaRepository<Users, Long> {
    /**
     * This is a Javadoc comment
     * @param usersAccountNumber accountnumber to be found in database
     * @return Users to response repository
     */
    Users findByUsersAccountNumber (Long usersAccountNumber);
}
