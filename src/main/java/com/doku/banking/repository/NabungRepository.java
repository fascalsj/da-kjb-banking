package com.doku.banking.repository;

import com.doku.banking.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface NabungRepository extends JpaRepository<Users, Long> {
}
