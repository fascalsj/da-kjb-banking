package com.doku.banking.repository;

import com.doku.banking.model.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface TransferRepository extends JpaRepository<Transfer, Long> {
}
