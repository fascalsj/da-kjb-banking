package com.doku.banking.repository;

import com.doku.banking.model.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface TransactionsRepository extends JpaRepository<Transactions, Long> {


}
