package com.doku.banking.exception;

/**
 * This is a Javadoc comment
 */
public class UserNotFoundException extends Exception {
    /**
     * This is a Javadoc comment
     * @param messages the parameter of the class
     */
    public UserNotFoundException (String messages){
        super(messages);
    }
}
