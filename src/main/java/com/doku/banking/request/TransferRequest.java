package com.doku.banking.request;

import lombok.Data;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
public class TransferRequest {

    private Long accountNumber;
    private String name;
    private Long recipientAccountNumber;
    private String recipientName;
    private String bankName;
    private Long transferAmount;
    private Date date;
}
