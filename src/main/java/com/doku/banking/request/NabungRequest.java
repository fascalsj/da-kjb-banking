package com.doku.banking.request;

import lombok.Data;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
public class NabungRequest {

    private Long accountNumber;
    private String name;
    private Long debit;
    private Date date;

}
