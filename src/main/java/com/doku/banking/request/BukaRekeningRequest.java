package com.doku.banking.request;


import lombok.Data;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
public class BukaRekeningRequest {

    private String identityNumber;
    private String fullName;
    private String gender;
    private Date dateOfBirth;
    private String address;
    private String zipCode;
    private String phoneNumber;
    private String email;
    private String religion;
    private String jobTitle;




}
