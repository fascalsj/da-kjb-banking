package com.doku.banking.model;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@Entity
@Table(name = "transactions")
public class Transactions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Untuk Menggenerate AutoIncrement
    private Long id;

    @NotNull
    @Column(name = "account_number")
    private Long transactionsAccountNumber;

    @NotNull
    @Column(name = "name")
    private String transactionsName;

    @NotNull
    @Column(name = "debit")
    private Long transactionsDebit;

    @NotNull
    @Column(name = "date")
    private Date transactionsDate;

}
