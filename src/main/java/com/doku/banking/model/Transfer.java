package com.doku.banking.model;


import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@Entity
@Table(name = "transfer")
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Untuk Menggenerate AutoIncrement
    private Long id;

    @NotNull
    @Column(name = "account_number")
    private Long transferAccountNumber;

    @NotNull
    @Column(name = "name")
    private String transferName;

    @NotNull
    @Column(name = "recipient_name")
    private String recipientName;

    @NotNull
    @Column(name = "recipient_account_number")
    private Long recipientAccountNumber ;

    @NotNull
    @Column(name = "bank_name")
    private String transferBankName;

    @NotNull
    @Column(name = "transfer_amount")
    private Long transferAmount;

    @NotNull
    @Column(name = "date")
    private Date transferDate;

}
