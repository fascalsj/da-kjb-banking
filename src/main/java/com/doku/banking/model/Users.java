package com.doku.banking.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@Entity
@Table(name = "users")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Untuk Menggenerate AutoIncrement
    private Long id;

    @NotNull
    @Column(name = "account_number")
    private Long usersAccountNumber;

    @NotNull
    @Column(name = "name")
    private String usersName;

    @NotNull
    @Column(name = "balance_amount")
    private Long usersBalanceAmount;

    @NotNull
    @Column(name = "date")
    private Date usersDate;

}

