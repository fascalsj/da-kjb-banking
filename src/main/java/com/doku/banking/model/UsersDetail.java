package com.doku.banking.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Setter
@Getter
@Entity
@Table(name = "users_detail")
public class UsersDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="userDetailId_seq") // Untuk Menggenerate AutoIncrement
    @SequenceGenerator(sequenceName = "user_detail_id_seq", initialValue=3, allocationSize = 1, name = "userDetailId_seq")
    private Long id;

    @NotNull
    @Column(name = "identity_number")
    private String identityNumber;

    @NotNull
    @Column(name = "full_name")
    private String fullName;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "religion")
    private String religion;

    @Column(name = "job_title")
    private String jobTitle;

}
