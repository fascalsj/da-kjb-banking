package com.doku.banking.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.Collections;

/**
 * This is a Javadoc comment
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**
            * This is a Javadoc comment
     * @return Docket untuk konfigurasi data
     * konfigurasi semua setting berada pada Docket disini menggunakan swagger2
     */

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error/*")))
                .paths(Predicates.not(PathSelectors.regex("/actuator")))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact(
                "Clarysha Graceya Tatipikalawan",
                "Belum Ada Web Nanti Nyusul Ya :D",
                "clarysha79@gmail.com");
        return new ApiInfo(
                "Membangun Rest API dengan SpringBoot",
                "Ini adalah implementasi swagger pada Rest Spring Boot",
                "Version 1.0.0",
                "Diperuntukan untuk belajar",
                contact,
                "Clarysha",
                "clarysha79@gmail.com",
                Collections.emptyList());
    }
}
