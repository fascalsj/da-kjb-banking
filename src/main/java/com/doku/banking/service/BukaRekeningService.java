package com.doku.banking.service;


import com.doku.banking.dto.BukaRekeningDTO;
import com.doku.banking.model.UsersDetail;
import com.doku.banking.repository.BukaRekeningRepository;
import com.doku.banking.request.BukaRekeningRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * This is a Javadoc comment
 */
@Component
@Slf4j
public class BukaRekeningService {

    @Autowired
    BukaRekeningRepository bukaRekeningRepository;

    /**
     * This is a Javadoc comment
     * @param request the parameter of the class
     * @return response for return bukarekening
     */
    public Response<BukaRekeningDTO> doBukaRekening(@RequestBody BukaRekeningRequest request){

        UsersDetail usersDetail = new UsersDetail();
        String accountNumber = request.getIdentityNumber().substring(3,9)+request.getPhoneNumber().substring(6,11);

        usersDetail.setIdentityNumber(request.getIdentityNumber());
        usersDetail.setFullName(request.getFullName());
        usersDetail.setGender(request.getGender());
        usersDetail.setDateOfBirth(request.getDateOfBirth());
        usersDetail.setAddress(request.getAddress());
        usersDetail.setZipCode(request.getZipCode());
        usersDetail.setPhoneNumber(request.getPhoneNumber());
        usersDetail.setEmail(request.getEmail());
        usersDetail.setJobTitle(request.getJobTitle());
        usersDetail.setReligion(request.getReligion());

        bukaRekeningRepository.save(usersDetail);

        BukaRekeningDTO bukaRekeningDTO = BukaRekeningDTO.builder()
                .fullName(request.getFullName())
                .accountNumber(accountNumber)
                .build();

        return Response.<BukaRekeningDTO>builder()
                .responseCode("0000")
                .responseMessage("Berhasil Buka Rekening")
                .data(bukaRekeningDTO)
                .build();
    }


}
