package com.doku.banking.service;

import com.doku.banking.dto.TransferDTO;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.model.Transfer;
import com.doku.banking.model.Users;
import com.doku.banking.repository.TransferRepository;
import com.doku.banking.repository.UsersRepository;
import com.doku.banking.request.TransferRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * This is a Javadoc comment
 */
@Component
@Slf4j
public class TransferService {

    @Autowired
    TransferRepository transferRepository;

    @Autowired
    UsersRepository usersRepository;

    /**
     * This is a Javadoc comment
     * @param transferRequest the parameter of the class
     * @return response for return transfer
     * @throws UserNotFoundException when user does not exist in database
     */
    public Response<TransferDTO> doTransfer(@RequestBody TransferRequest transferRequest) throws UserNotFoundException {
        Users user = usersRepository.findByUsersAccountNumber(transferRequest.getAccountNumber());
        Transfer transfer = new Transfer();

        Users userRecipient =usersRepository.findByUsersAccountNumber(transferRequest.getRecipientAccountNumber());

        if (user == null){

            throw new UserNotFoundException("User with this account number is not found");
        }

        if (user.getUsersBalanceAmount() <= transferRequest.getTransferAmount()){

            throw new ArithmeticException("Insufficient Balance, Your Balance is "+ user.getUsersBalanceAmount());

        }

        if (userRecipient == null){

            throw new UserNotFoundException("User recipient is not found");
        }


        transfer.setTransferAccountNumber(transferRequest.getAccountNumber());
        transfer.setTransferName(transferRequest.getName());
        transfer.setTransferBankName(transferRequest.getBankName());
        transfer.setRecipientAccountNumber(transferRequest.getRecipientAccountNumber());
        transfer.setRecipientName(transferRequest.getRecipientName());
        transfer.setTransferAmount(transferRequest.getTransferAmount());
        transfer.setTransferDate(transferRequest.getDate());

        transferRepository.save(transfer);

        user.setUsersBalanceAmount(user.getUsersBalanceAmount()-transferRequest.getTransferAmount()-6500);
        usersRepository.save(user);

        userRecipient.setUsersBalanceAmount(userRecipient.getUsersBalanceAmount()+transferRequest.getTransferAmount());
        usersRepository.save(userRecipient);

        TransferDTO transferDTO = TransferDTO.builder()
                .accountNumber(transferRequest.getAccountNumber())
                .name(transferRequest.getName())
                .recipientAccountNumber(transferRequest.getRecipientAccountNumber())
                .recipientName(transferRequest.getRecipientName())
                .bankName(transferRequest.getBankName())
                .transferAmount(transferRequest.getTransferAmount())
                .date(transferRequest.getDate())
                .build();

        return Response.<TransferDTO>builder()
                .responseCode("0000")
                .responseMessage("Transfer Success")
                .data(transferDTO)
                .build();
    }

}
