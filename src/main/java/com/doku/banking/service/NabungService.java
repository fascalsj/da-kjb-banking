package com.doku.banking.service;

import com.doku.banking.dto.NabungDTO;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.model.Transactions;
import com.doku.banking.model.Users;
import com.doku.banking.repository.NabungRepository;
import com.doku.banking.repository.TransactionsRepository;
import com.doku.banking.repository.UsersRepository;
import com.doku.banking.request.NabungRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * This is a Javadoc comment
 */
@Component
@Slf4j
public class NabungService {

    @Autowired
    NabungRepository nabungRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    TransactionsRepository transactionsRepository;

    /**
     * This is a Javadoc comment
     * @param nabungRequest the parameter of the class
     * @return response for return nabung
     * @throws UserNotFoundException when user does not exist in database
     */
    public Response<NabungDTO> doNabung(@RequestBody NabungRequest nabungRequest) throws UserNotFoundException {
        Users user = usersRepository.findByUsersAccountNumber(nabungRequest.getAccountNumber());

        if (user == null){

            throw new UserNotFoundException("User with this account number is not found");
        }

        user.setUsersBalanceAmount(nabungRequest.getDebit()+user.getUsersBalanceAmount());

        usersRepository.save(user);

        Transactions transactions = new Transactions();
        transactions.setTransactionsDebit(nabungRequest.getDebit());
        transactions.setTransactionsAccountNumber(nabungRequest.getAccountNumber());
        transactions.setTransactionsName(nabungRequest.getName());
        transactions.setTransactionsDate(nabungRequest.getDate());
        transactionsRepository.save(transactions);

        NabungDTO nabungDTO = NabungDTO.builder()
                .accountNumber(nabungRequest.getAccountNumber())
                .name(nabungRequest.getName())
                .balanceAmount(nabungRequest.getDebit())
                .date(nabungRequest.getDate())
                .build();

        return Response.<NabungDTO>builder()
                .responseCode("0000")
                .responseMessage("Berhasil Menabung")
                .data(nabungDTO)
                .build();

    }

}
