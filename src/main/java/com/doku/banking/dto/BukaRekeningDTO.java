package com.doku.banking.dto;

import lombok.Builder;
import lombok.Data;

/**
 * This is a Javadoc comment
 */
@Data
@Builder
public class BukaRekeningDTO {

    private String fullName;
    private String accountNumber;
}
