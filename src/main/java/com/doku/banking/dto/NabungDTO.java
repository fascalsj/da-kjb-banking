package com.doku.banking.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Getter
@Builder
public class NabungDTO {

    private Long accountNumber;
    private String name;
    private Long balanceAmount;
    private Date date;
}
