package com.doku.banking.dto;

import lombok.Builder;
import lombok.Data;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@Builder
public class TransferDTO {

    private Long accountNumber;
    private String name;
    private Long recipientAccountNumber;
    private String recipientName;
    private String bankName;
    private Long transferAmount;
    private Date date;
}
