package com.doku.banking.util;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * This is a Javadoc comment
 * @param <T> the parameter of the class
 */
@Setter /*Setter*/
@Getter /*Getter*/
@Builder
public class Response<T> {
    private String responseCode;
    private String responseMessage;
    private T data;
}
