INSERT INTO USERS_DETAIL(id, identity_number, full_name, gender, address, date_of_birth, zip_code, phone_number, email, religion, job_title)
values (1, '1304045306960001', 'Clarysha Graceya Tatipikalawan','Female','South Jakarta', '1996-06-13', '12210', '081288912205', 'clarysha79@gmail.com', 'islam', 'software engineer');

INSERT INTO USERS_DETAIL(id, identity_number, full_name, gender, address, date_of_birth, zip_code, phone_number, email, religion, job_title)
values (2, '130404530695001', 'Clacendya Fellicya Tatipikalawan','Female','Padang', '2000-09-04', '12210', '081288912354', 'clacendya@gmail.com', 'islam', 'Student');

INSERT INTO USERS(id, account_number, name, balance_amount, date)
values (1, '40453091220', 'Clarysha Graceya Tatipikalawan', 3000000, current_timestamp);

INSERT INTO USERS(id, account_number, name, balance_amount, date)
values (2, '40453091235', 'Clacendya Fellicya Tatipikalawan', 2000000, current_timestamp);

