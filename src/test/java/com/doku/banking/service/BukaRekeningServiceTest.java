package com.doku.banking.service;

import com.doku.banking.BankingApplication;
import com.doku.banking.controller.BukaRekeningController;
import com.doku.banking.request.BukaRekeningRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BukaRekeningServiceTest {


    @Autowired
    BukaRekeningController bukaRekeningController;

    @Test
    public void doBukaRekening() throws ParseException {

            BukaRekeningRequest bukaRekeningRequest = new BukaRekeningRequest();
            String date = "1996-06-13";
            SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD");
            Date DOB = format.parse(date);

            bukaRekeningRequest.setIdentityNumber("1304045306960001");
            bukaRekeningRequest.setFullName("Clarysha Graceya Tatipikalawan");
            bukaRekeningRequest.setDateOfBirth(DOB);
            bukaRekeningRequest.setGender("Female");
            bukaRekeningRequest.setAddress("South Jakarta");
            bukaRekeningRequest.setZipCode("12210");
            bukaRekeningRequest.setPhoneNumber("081288912205");
            bukaRekeningRequest.setReligion("Islam");
            bukaRekeningRequest.setJobTitle("Software Engineer");

            Response response = bukaRekeningController.doBukaRekening(bukaRekeningRequest);
            assertEquals("0000", response.getResponseCode());
            assertEquals("Berhasil Buka Rekening", response.getResponseMessage());

    }


}