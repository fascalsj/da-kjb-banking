package com.doku.banking.service;

import com.doku.banking.controller.NabungController;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.request.NabungRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class NabungServiceTest {

    @Autowired
    NabungController nabungController;

    @Test(expected = UserNotFoundException.class)
    public void doNabung_failed() throws UserNotFoundException {
        NabungRequest nabungRequest = new NabungRequest();

        nabungRequest.setAccountNumber(4045091220L);
        nabungRequest.setName("Clarysha Graceya Tatipikalawan");
        nabungRequest.setDebit(200000L);
        nabungRequest.setDate(new Date());
        Response response = nabungController.doNabung(nabungRequest);

    }

    @Test
    public void doNabung_success() throws UserNotFoundException {
        NabungRequest nabungRequest = new NabungRequest();

        nabungRequest.setAccountNumber(40453091220L);
        nabungRequest.setName("Clarysha Graceya Tatipikalawan");
        nabungRequest.setDebit(200000L);
        nabungRequest.setDate(new Date());
        Response response = nabungController.doNabung(nabungRequest);
        assertEquals("0000", response.getResponseCode());
        assertEquals("Berhasil Menabung", response.getResponseMessage());

    }
}