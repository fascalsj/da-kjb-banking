package com.doku.banking.service;

import com.doku.banking.controller.BukaRekeningController;
import com.doku.banking.dto.BukaRekeningDTO;
import com.doku.banking.dto.NabungDTO;
import com.doku.banking.dto.TransferDTO;
import com.doku.banking.model.Transfer;
import com.doku.banking.model.UsersDetail;
import com.doku.banking.request.BukaRekeningRequest;
import com.doku.banking.util.Response;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ModelTest {

    @Test
    public void userDetail () throws ParseException {

            String identityNumber = "1304045306960001";
            String fullName = "Clarysha Graceya Tatipikalawan";
            String gender = "Female";
            String address = "South Jakarta";
            String zipCode = "12210";
            String phoneNumber = "081288912205";
            String email = "clarysha79@gmail.com";
            String religion = "Islam";
            String jobTitle = "Quality Assurance";
            String date = "1996-06-13";
            SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD");
            Date DOB = format.parse(date);


            UsersDetail usersDetail = new UsersDetail();
            usersDetail.setIdentityNumber(identityNumber);
            usersDetail.setFullName(fullName);
            usersDetail.setDateOfBirth(DOB);
            usersDetail.setGender(gender);
            usersDetail.setAddress(address);
            usersDetail.setPhoneNumber(phoneNumber);
            usersDetail.setZipCode(zipCode);
            usersDetail.setEmail(email);
            usersDetail.setReligion(religion);
            usersDetail.setJobTitle(jobTitle);

            assertEquals(identityNumber, usersDetail.getIdentityNumber());
            assertEquals(fullName, usersDetail.getFullName());
            assertEquals(DOB.toString(), usersDetail.getDateOfBirth().toString());
            assertEquals(gender, usersDetail.getGender());
            assertEquals(address, usersDetail.getAddress());
            assertEquals(phoneNumber, usersDetail.getPhoneNumber());
            assertEquals(email, usersDetail.getEmail());
            assertEquals(religion, usersDetail.getReligion());
            assertEquals(jobTitle, usersDetail.getJobTitle());
            assertEquals(zipCode, usersDetail.getZipCode());


    }

    @Test
    public void bukaRekeningDTO (){



            String accountNumber = "40453091220";
            String fullName = "Clarysha Graceya Tatipikalawan";


            BukaRekeningDTO bukaRekeningDTO = BukaRekeningDTO.builder()
                    .fullName(fullName)
                    .accountNumber(accountNumber)
                    .build();

            assertEquals(accountNumber, bukaRekeningDTO.getAccountNumber());
            assertEquals(fullName, bukaRekeningDTO.getFullName());
    }

    @Test
    public void nabungDTO(){

            Long accountNumber = 40453091220L;
            String name = "Clarysha Graceya Tatipikalawan";
            Long balanceAmount = 300000L;
            Date date = new Date();

            NabungDTO nabungDTO = NabungDTO.builder()
                    .accountNumber(accountNumber)
                    .name(name)
                    .balanceAmount(balanceAmount)
                    .date(date)
                    .build();

            assertEquals(accountNumber.toString(), nabungDTO.getAccountNumber().toString());
            assertEquals(name, nabungDTO.getName());
            assertEquals(balanceAmount.toString(), nabungDTO.getBalanceAmount().toString());
            assertEquals(date.toString(), nabungDTO.getDate().toString());
    }

    @Test
    public void transferDTO(){

            Long accountNumber = 40453091220L;
            String name = "Clarysha Graceya Tatipikalawan";
            Long recipientAccountNumber = 40453091235L;
            String recipientName = "Clacendya Fellicya Tatipikalawan";
            String bankName = "BCA";
            Long transferAmount = 200000L;
            Date date = new Date();

            TransferDTO transferDTO = TransferDTO.builder()
                    .accountNumber(accountNumber)
                    .name(name)
                    .recipientAccountNumber(recipientAccountNumber)
                    .recipientName(recipientName)
                    .bankName(bankName)
                    .transferAmount(transferAmount)
                    .date(date)
                    .build();

            assertEquals(accountNumber.toString(), transferDTO.getAccountNumber().toString());
            assertEquals(name, transferDTO.getName());
            assertEquals(recipientAccountNumber.toString(), transferDTO.getRecipientAccountNumber().toString());
            assertEquals(recipientName, transferDTO.getRecipientName());
            assertEquals(bankName, transferDTO.getBankName());
            assertEquals(transferAmount, transferDTO.getTransferAmount());
            assertEquals(date.toString(), transferDTO.getDate().toString());

    }

}