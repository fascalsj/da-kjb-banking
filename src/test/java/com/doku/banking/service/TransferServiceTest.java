package com.doku.banking.service;

import com.doku.banking.controller.TransferController;
import com.doku.banking.exception.UserNotFoundException;
import com.doku.banking.request.NabungRequest;
import com.doku.banking.request.TransferRequest;
import com.doku.banking.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TransferServiceTest {
    @Autowired
    TransferController transferController;

    @Test(expected = UserNotFoundException.class)
    public void doTransfer_failed_account_sender() throws UserNotFoundException {
        TransferRequest transferRequest = new TransferRequest();

        transferRequest.setAccountNumber(40463091220L);
        transferRequest.setName("Clarysha Graceya Tatipikalawan");
        transferRequest.setBankName("BCA");
        transferRequest.setRecipientAccountNumber(43453091235L);
        transferRequest.setRecipientName("Clacendya Fellicya Tatipikalawan");
        transferRequest.setTransferAmount(200000L);
        transferRequest.setDate(new Date());
        Response response = transferController.doTransfer(transferRequest);

    }

    @Test(expected = UserNotFoundException.class)
    public void doTransfer_failed_account_recipient() throws UserNotFoundException {
        TransferRequest transferRequest = new TransferRequest();

        transferRequest.setAccountNumber(40453091220L);
        transferRequest.setName("Clarysha Graceya Tatipikalawan");
        transferRequest.setBankName("BCA");
        transferRequest.setRecipientAccountNumber(43443091235L);
        transferRequest.setRecipientName("Clacendya Fellicya Tatipikalawan");
        transferRequest.setTransferAmount(200000L);
        transferRequest.setDate(new Date());
        Response response = transferController.doTransfer(transferRequest);

    }

    @Test
    public void doTransfer_success() throws UserNotFoundException {
        TransferRequest transferRequest = new TransferRequest();

        transferRequest.setAccountNumber(40453091220L);
        transferRequest.setName("Clarysha Graceya Tatipikalawan");
        transferRequest.setBankName("BCA");
        transferRequest.setRecipientAccountNumber(40453091235L);
        transferRequest.setRecipientName("Clacendya Fellicya Tatipikalawan");
        transferRequest.setTransferAmount(200000L);
        transferRequest.setDate(new Date());
        Response response = transferController.doTransfer(transferRequest);
        assertEquals("0000", response.getResponseCode());
        assertEquals("Transfer Success", response.getResponseMessage());
    }

    @Test(expected = ArithmeticException.class)
    public void doTransfer_insufficient_balance() throws UserNotFoundException {
        TransferRequest transferRequest = new TransferRequest();

        transferRequest.setAccountNumber(40453091220L);
        transferRequest.setName("Clarysha Graceya Tatipikalawan");
        transferRequest.setBankName("BCA");
        transferRequest.setRecipientAccountNumber(43443091235L);
        transferRequest.setRecipientName("Clacendya Fellicya Tatipikalawan");
        transferRequest.setTransferAmount(40000000L);
        transferRequest.setDate(new Date());
        Response response = transferController.doTransfer(transferRequest);

    }

}